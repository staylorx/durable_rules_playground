package main

import (
	"dagger.io/dagger"
	"universe.dagger.io/docker"
)

project_name:    "durable_rules_playground"
project_workdir: "/workdir/" + project_name

dagger.#Plan & {

	client: {
		filesystem: {
			"./": read: contents:                          dagger.#FS
			"./project.cue": write: contents:              actions.lint.cue.export.files."/workdir/project.cue"
			"./durable_rules_playground": write: contents: actions.lint.bandit.export.directories."/workdir/durable_rules_playground"
		}
	}
	python_version: string | *"3.10"
	poetry_version: string | *"1.1.14"

	actions: {
		// referential build for base python image
		_python_pre_build: docker.#Build & {
			steps: [
				docker.#Pull & {
					source: "python:" + python_version
				},
				docker.#Run & {
					command: {
						name: "mkdir"
						args: ["/workdir"]
					}
				},
				docker.#Copy & {
					contents: client.filesystem."./".read.contents
					source:   "./pyproject.toml"
					dest:     "/workdir/pyproject.toml"
				},
				docker.#Copy & {
					contents: client.filesystem."./".read.contents
					source:   "./poetry.lock"
					dest:     "/workdir/poetry.lock"
				},
				docker.#Run & {
					workdir: "/workdir"
					command: {
						name: "pip"
						args: ["install", "--no-cache-dir", "poetry==" + poetry_version]
					}
				},
				docker.#Set & {
					config: {
						env: ["POETRY_VIRTUALENVS_CREATE"]: "false"
					}
				},
				docker.#Run & {
					workdir: "/workdir"
					command: {
						name: "poetry"
						args: ["install", "--no-interaction", "--no-ansi"]
					}
				},
			]
		}
		// python build for actions in this plan
		_python_build: docker.#Build & {
			steps: [
				docker.#Copy & {
					input:    _python_pre_build.output
					contents: client.filesystem."./".read.contents
					source:   "./"
					dest:     "/workdir"
				},
			]
		}
		// cuelang pre build
		_cue_pre_build: docker.#Build & {
			steps: [
				docker.#Pull & {
					source: "golang:latest"
				},
				docker.#Run & {
					command: {
						name: "mkdir"
						args: ["/workdir"]
					}
				},
				docker.#Run & {
					command: {
						name: "go"
						args: ["install", "cuelang.org/go/cmd/cue@latest"]
					}
				},
			]
		}
		// cuelang build for actions in this plan
		_cue_build: docker.#Build & {
			steps: [
				docker.#Copy & {
					input:    _cue_pre_build.output
					contents: client.filesystem."./".read.contents
					source:   "./project.cue"
					dest:     "/workdir/project.cue"
				},
			]
		}
		// lint code and file formatting
		lint: {
			// mypy static type check
			mypy: docker.#Run & {
				input:   _python_build.output
				workdir: "/workdir"
				command: {
					name: "poetry"
					args: ["run", "mypy", project_name, "--ignore-missing-imports"]
				}
			}
			// sort python imports with isort
			isort: docker.#Run & {
				input:   mypy.output
				workdir: "/workdir"
				command: {
					name: "poetry"
					args: ["run", "isort", project_name]
				}
			}
			// code style formatting with black
			black: docker.#Run & {
				input:   isort.output
				workdir: "/workdir"
				command: {
					name: "poetry"
					args: ["run", "black", project_name]
				}
			}
			// pylint checks
			pylint: docker.#Run & {
				input:   black.output
				workdir: "/workdir"
				command: {
					name: "poetry"
					args: ["run", "pylint", project_name]
				}
			}
			// safety security vulnerabilities check
			safety: docker.#Run & {
				input:   pylint.output
				workdir: "/workdir"
				command: {
					name: "poetry"
					args: ["run", "safety"]
				}
			}
			// bandit security vulnerabilities check
			bandit: docker.#Run & {
				input:   safety.output
				workdir: "/workdir"
				command: {
					name: "poetry"
					args: ["run", "bandit", project_name]
				}
				export: {
					directories: {
						"/workdir/durable_rules_playground": _
					}
				}
			}

			// code formatting for cuelang
			cue: docker.#Run & {
				input:   _cue_build.output
				workdir: "/workdir"
				command: {
					name: "cue"
					args: ["fmt", "/workdir/project.cue"]
				}
				export: {
					files: "/workdir/project.cue": _
				}
			}
		}
		//run test suites
		test: {
			// sort python imports with isort
			pytest: docker.#Run & {
				input:   _python_build.output
				workdir: "/workdir"
				command: {
					name: "poetry"
					args: ["run", "pytest"]
				}
			}
		}
	}
}
