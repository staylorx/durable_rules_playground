from durable.lang import m, post, ruleset, when_all

with ruleset("test"):

    @when_all(m.subject == "World")
    def say_hello(c):
        print(f"Hello {c.m.subject}")


post("test", {"subject": "World"})
