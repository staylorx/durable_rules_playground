from durable.lang import ruleset, when_all, m, assert_fact


def test_ruleset_invalid1():

    try:
        with ruleset("invalid1"):

            @when_all(m.subject.matches("*"))
            def should_not_work(c):
                print("should not work: {0}".format(c.m.subject))

        assert_fact("invalid1", {"subject": "hello world"})
    except BaseException as e:
        print("invalid1 -> expected {0}".format(e))


def test_ruleset_invalid2():

    try:
        with ruleset("invalid2"):

            @when_all(m.subject.matches(".**"))
            def should_not_work(c):
                print("should not work: {0}".format(c.m.subject))

        assert_fact("invalid2", {"subject": "hello world"})
    except BaseException as e:
        print("invalid2 -> expected {0}".format(e))


def test_ruleset_invalid3():

    try:
        with ruleset("invalid3"):

            @when_all(m.subject.matches(".?*"))
            def should_not_work(c):
                print("should not work: {0}".format(c.m.subject))

        assert_fact("invalid3", {"subject": "hello world"})
    except BaseException as e:
        print("invalid3 -> expected {0}".format(e))
