from durable.lang import ruleset, m, post, when_all


def test_ruleset_nested():

    with ruleset("nested"):

        @when_all(+m.item)
        def test(c):
            print("nested ->{0}".format(c.m.item))

    post("nested", {"item": "not_nested"})
    post("nested", {"item": {"nested": "true"}})
    post("nested", {"item": {"nested": {"nested": "true"}}})
