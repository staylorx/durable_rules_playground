import threading

from durable.lang import ruleset, when_all, s, update_state, m, post


def test_ruleset_flow():

    with ruleset("flow"):
        # state condition uses 's'
        @when_all(s.status == "start")
        def start(c):
            # state update on 's'
            c.s.status = "next"
            print("flow-> start")

        @when_all(s.status == "next")
        def next(c):
            c.s.status = "last"
            print("flow-> next")

        @when_all(s.status == "last")
        def last(c):
            c.s.status = "end"
            print("flow-> last")
            # deletes state at the end
            c.delete_state()

    # modifies default context state
    update_state("flow", {"status": "start"})


def test_ruleset_flow0():

    with ruleset("flow0"):

        def start_timer(time, callback):
            timer = threading.Timer(time, callback)
            timer.daemon = True
            timer.start()

        @when_all(s.state == "first")
        # async actions take a callback argument to signal completion
        def first(c, complete):
            def end_first():
                c.s.state = "second"
                print("flow0-> first completed")

                # completes the action after 3 seconds
                complete(None)

            start_timer(3, end_first)

        @when_all(s.state == "second")
        def second(c, complete):
            def end_second():
                c.s.state = "third"
                print("flow0-> second completed")

                # completes the action after 6 seconds
                # use the first argument to signal an error
                complete(Exception("error detected"))

            start_timer(10, end_second)

            # overrides the 5 second default abandon timeout
            return 15

    update_state("flow0", {"state": "first"})


def test_ruleset_flow1():

    with ruleset("flow1"):

        @when_all(m.action == "start")
        def first(c):
            raise Exception("Unhandled Exception!")

        # when the exception property exists
        @when_all(+s.exception)
        def second(c):
            print("flow1-> expected {0}".format(c.s.exception))
            c.s.exception = None

    post("flow1", {"action": "start"})
