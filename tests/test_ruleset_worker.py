from durable.lang import m, post, when_all, state, statechart, to


def test_ruleset_worker():

    with statechart("worker"):
        # super-state 'work' has two states and one trigger
        with state("work"):
            # sub-state 'enter' has only one trigger
            with state("enter"):

                @to("process")
                @when_all(m.subject == "enter")
                def continue_process(c):
                    print("worker-> start process")
                    print("worker-> {0}".format(c.get_pending_events()))

            with state("process"):

                @to("process")
                @when_all(m.subject == "continue")
                def continue_process(c):
                    print("worker-> continue processing")

            # the super-state trigger will be evaluated for all sub-state triggers
            @to("canceled")
            @when_all(m.subject == "cancel")
            def cancel(c):
                print("worker-> cancel process")

        state("canceled")

    # will move the statechart to the 'work.process' sub-state
    post("worker", {"subject": "enter"})

    # will keep the statechart to the 'work.process' sub-state
    post("worker", {"subject": "continue"})
    post("worker", {"subject": "continue"})

    # will move the statechart out of the work state
    post("worker", {"subject": "cancel"})
