from durable.lang import *


def test_ruleset_coerce():

    with ruleset("coerce"):

        @when_all(m.subject.matches("10.500000"))  # type: ignore
        def starts_with(c):
            print("coerce-> matches 10.500000: {0}".format(c.m.subject))

        @when_all(m.subject.matches("5"))  # type: ignore
        def starts_with(c):
            print("coerce-> matches 5: {0}".format(c.m.subject))

        @when_all(m.subject.matches("false"))  # type: ignore
        def starts_with(c):
            print("coerce-> matches false: {0}".format(c.m.subject))

        @when_all(m.subject.matches("nil"))  # type: ignore
        def starts_with(c):
            print("coerce-> matches nil")

    assert_fact("coerce", {"subject": 10.5})
    assert_fact("coerce", {"subject": 5})
    assert_fact("coerce", {"subject": False})
    assert_fact("coerce", {"subject": None})
