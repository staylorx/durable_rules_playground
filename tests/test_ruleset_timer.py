import datetime

from durable.lang import ruleset, when_all, m, timeout, post, when_any, s, update_state


def test_ruleset_timer1():

    with ruleset("timer1"):

        @when_all(m.subject == "start")
        def start(c):
            c.start_timer("MyTimer", 5)

        @when_all(timeout("MyTimer"))
        def timer(c):
            print("timer1 timeout")

    post("timer1", {"subject": "start"})


def test_ruleset_timer2():

    with ruleset("timer2"):
        # will trigger when MyTimer expires
        @when_any(all(s.count == 0), all(s.count < 5, timeout("MyTimer")))  # type: ignore
        def pulse(c):
            c.s.count += 1
            # MyTimer will expire in 5 seconds
            c.start_timer("MyTimer", 1)
            print(
                "timer2 pulse ->{0}".format(
                    datetime.datetime.now().strftime("%I:%M:%S%p")
                )
            )

        @when_all(m.cancel == True)
        def cancel(c):
            c.cancel_timer("MyTimer")
            print("timer2 canceled timer")

    update_state("timer2", {"count": 0})
