from durable.lang import ruleset, when_all, m, post


def test_ruleset_match():

    with ruleset("match"):

        @when_all(
            m.url.matches("(https?://)?([0-9a-z.-]+)%.[a-z]{2,6}(/[A-z0-9_.-]+/?)*")
        )
        def approved(c):
            print("match-> url {0}".format(c.m.url))

    post("match", {"url": "https://github.com"})
    try:
        post("match", {"url": "http://github.com/jruizgit/rul!es"})
    except BaseException as e:
        print("match -> expected {0}".format(e))
    post("match", {"url": "https://github.com/jruizgit/rules/reference.md"})

    def match_callback(e, state):
        print("match -> expected {0}".format(e.message))

    post("match", {"url": "//rules"}, match_callback)
    post("match", {"url": "https://github.c/jruizgit/rules"}, match_callback)
