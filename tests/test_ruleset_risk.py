from durable.lang import (
    ruleset,
    when_all,
    c,
    m,
    post,
    none,
    statechart,
    to,
    state,
    count,
    timeout,
    item,
    cap,
    get_host,
)


def test_ruleset_risk():

    with ruleset("risk"):

        @when_all(
            c.first << m.t == "purchase", c.second << m.location != c.first.location
        )
        # the event pair will only be observed once
        def fraud(c):
            print(
                "risk-> Fraud detected -> {0}, {1}".format(
                    c.first.location, c.second.location
                )
            )

    # 'post' submits events, try 'assert' instead and to see differt behavior
    post("risk", {"t": "purchase", "location": "US"})
    post("risk", {"t": "purchase", "location": "CA"})


def test_ruleset_risk1():

    with ruleset("risk1"):

        @when_all(
            c.first << m.t == "deposit",
            none(m.t == "balance"),
            c.third << m.t == "withdrawal",
            c.fourth << m.t == "chargeback",
        )
        def detected(c):
            print(
                "risk1-> fraud detected {0} {1} {2}".format(
                    c.first.t, c.third.t, c.fourth.t
                )
            )

    post("risk1", {"t": "deposit"})
    post("risk1", {"t": "withdrawal"})
    post("risk1", {"t": "chargeback"})


def test_ruleset_risk3():

    with statechart("risk3"):
        with state("start"):

            @to("meter")
            def start(c):
                c.start_timer("RiskTimer", 5)

        with state("meter"):

            @to("fraud")
            @when_all(count(3), c.message << m.amount > 100)
            def fraud(c):
                for e in c.m:
                    print("risk3-> {0}".format(e.message))

            @to("exit")
            @when_all(timeout("RiskTimer"))
            def exit(c):
                print("risk3-> exit")

        state("fraud")
        state("exit")

    # three events in a row will trigger the fraud rule
    post("risk3", {"amount": 200})
    post("risk3", {"amount": 300})
    post("risk3", {"amount": 400})

    # two events will exit after 5 seconds
    post("risk3", {"sid": 1, "amount": 500})
    post("risk3", {"sid": 1, "amount": 600})


def test_ruleset_risk4():

    with statechart("risk4"):
        with state("start"):

            @to("meter")
            def start(c):
                c.start_timer("VelocityTimer", 5, True)

        with state("meter"):

            @to("meter")
            @when_all(cap(5), m.amount > 100, timeout("VelocityTimer"))
            def some_events(c):
                print("velocity: {0} in 5 seconds".format(len(c.m)))
                # resets and restarts the manual reset timer
                c.start_timer("VelocityTimer", 5, True)

            @to("meter")
            @when_all(timeout("VelocityTimer"))
            def no_events(c):
                print("velocity: no events in 5 seconds")
                c.cancel_timer("VelocityTimer")

    post("risk4", {"amount": 200})
    post("risk4", {"amount": 300})
    post("risk4", {"amount": 500})
    post("risk4", {"amount": 600})


def test_ruleset_risk5():

    with ruleset("risk5"):
        # compares properties in the same event, this expression is evaluated in the client
        @when_all(m.debit > m.credit * 2)
        def fraud_1(c):
            print(
                "risk5-> debit {0} more than twice the credit {1}".format(
                    c.m.debit, c.m.credit
                )
            )

        # compares two correlated events, this expression is evaluated in the backend
        @when_all(
            c.first << m.amount > 100,
            c.second << m.amount > c.first.amount + m.amount / 2,
        )
        def fraud_2(c):
            print("risk5-> fraud detected ->{0}".format(c.first.amount))
            print("risk5-> fraud detected ->{0}".format(c.second.amount))

    post("risk5", {"debit": 220, "credit": 100})

    try:
        post("risk5", {"debit": 150, "credit": 100})
    except BaseException as e:
        print("risk5 expected {0}".format(e))

    post("risk5", {"amount": 200})
    post("risk5", {"amount": 500})


def test_ruleset_risk6():

    with ruleset("risk6"):
        # matching primitive array
        @when_all(m.payments.allItems((item > 100) & (item < 400)))  # type: ignore
        def rule1(c):
            print("risk6-> should not match {0}".format(c.m.payments))

        # matching primitive array
        @when_all(m.payments.allItems((item > 100) & (item < 500)))  # type: ignore
        def rule1(c):
            print("risk6-> fraud 1 detected {0}".format(c.m.payments))

        # matching object array
        @when_all(m.payments.allItems((item.amount < 250) | (item.amount >= 300)))
        def rule2(c):
            print("risk6-> fraud 2 detected {0}".format(c.m.payments))

        # pattern matching string array
        @when_all(m.cards.anyItem(item.matches("three.*")))
        def rule3(c):
            print("risk6-> fraud 3 detected {0}".format(c.m.cards))

        # matching nested arrays
        @when_all(m.payments.anyItem(item.allItems(item < 100)))
        def rule4(c):
            print("risk6-> fraud 4 detected {0}".format(c.m.payments))

        @when_all(m.payments.allItems(item > 100) & (m.cash == True))
        def rule5(c):
            print("risk6-> fraud 5 detected {0}".format(c.m.cash))

        @when_all(
            (m.field == 1)
            & m.payments.allItems(item.allItems((item > 100) & (item < 1000)))
        )
        def rule6(c):
            print("risk6-> fraud 6 detected {0}".format(c.m.payments))

        @when_all(
            (m.field == 1)
            & m.payments.allItems(item.anyItem((item > 100) | (item < 50)))
        )
        def rule7(c):
            print("risk6-> fraud 7 detected {0}".format(c.m.payments))

        @when_all(m.payments.anyItem(-item.field1 & (item.field2 == 2)))
        def rule8(c):
            print("risk6-> fraud 8 detected {0}".format(c.m.payments))

        @when_all(
            m.features.feature1.anyItem(item == "a") | (m.features.feature2 == "c")
        )
        def rule9(c):
            print("risk6-> fraud 9 detected {0}".format(c.m.features))

    def risk6_callback(e, state):
        print("risk6 -> expected {0}".format(e.message))

    post("risk6", {"payments": [150, 300, 450]})
    post("risk6", {"payments": [{"amount": 200}, {"amount": 300}, {"amount": 450}]})
    post("risk6", {"cards": ["one card", "two cards", "three cards"]})
    post("risk6", {"payments": [[10, 20, 30], [30, 40, 50], [10, 20]]})
    post("risk6", {"payments": [150, 350, 600], "cash": True})
    post("risk6", {"field": 1, "payments": [[200, 300], [150, 200]]})
    post("risk6", {"field": 1, "payments": [[20, 180], [90, 190]]})
    post("risk6", {"payments": [{"field2": 2}]})
    post("risk6", {"payments": [{"field2": 1}]}, risk6_callback)
    post("risk6", {"payments": [{"field1": 1, "field2": 2}]}, risk6_callback)
    post("risk6", {"payments": [{"field1": 1, "field2": 1}]}, risk6_callback)
    post("risk6", {"features": {"feature1": ["a"], "feature2": "c"}})
    post("risk6", {"features": {"feature1": [], "feature2": "c"}})

    def risk7_callback(c):
        print("risk7 fraud detected")

    get_host().set_rulesets(
        {
            "risk7": {
                "suspect": {
                    "run": risk7_callback,
                    "all": [
                        {"first": {"t": "purchase"}},
                        {"second": {"$neq": {"location": {"first": "location"}}}},
                    ],
                }
            }
        }
    )

    post("risk7", {"t": "purchase", "location": "US"})
    post("risk7", {"t": "purchase", "location": "CA"})
