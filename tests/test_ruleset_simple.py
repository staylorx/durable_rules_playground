from durable.lang import ruleset, when_all, m, post


def test_ruleset_simple():

    with ruleset("test"):
        # antecedent
        @when_all(m.subject == "World")
        def say_hello(c):
            # consequent
            print("test-> Hello {0}".format(c.m.subject))

    post("test", {"subject": "World"})


def test_ruleset_none():

    with ruleset("none_test"):

        @when_all(m.v is None)
        def is_none(c):
            print("none_test passed")

    post("none_test", {"v": None})
    try:
        post("none_test", {"v": "something"})
    except BaseException as e:
        print("none_test -> expected {0}".format(e))
