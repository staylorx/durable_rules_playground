from durable.lang import ruleset, when_all, m, none, assert_fact, post, retract_fact


def test_ruleset_bookstore():

    with ruleset("bookstore"):
        # this rule will trigger for events with status
        @when_all(+m.status)
        def event(c):
            print(
                "bookstore-> Reference {0} status {1}".format(c.m.reference, c.m.status)
            )

        @when_all(+m.name)
        def fact(c):
            print("bookstore-> Added {0}".format(c.m.name))

        # this rule will be triggered when the fact is retracted
        @when_all(none(+m.name))
        def empty(c):
            print("bookstore-> No books")

    assert_fact(
        "bookstore",
        {
            "name": "The new book",
            "seller": "bookstore",
            "reference": "75323",
            "price": 500,
        },
    )
    # will throw MessageObservedError because the fact has already been asserted
    try:
        assert_fact(
            "bookstore",
            {
                "reference": "75323",
                "name": "The new book",
                "price": 500,
                "seller": "bookstore",
            },
        )
    except BaseException as e:
        print("bookstore expected {0}".format(e))
    post("bookstore", {"reference": "75323", "status": "Active"})
    post("bookstore", {"reference": "75323", "status": "Active"})
    retract_fact(
        "bookstore",
        {
            "reference": "75323",
            "name": "The new book",
            "price": 500,
            "seller": "bookstore",
        },
    )
