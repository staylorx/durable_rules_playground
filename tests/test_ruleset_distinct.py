from durable.lang import c, ruleset, when_all, distinct, m, post


def test_ruleset_indistinct():

    with ruleset("indistinct"):

        @when_all(
            distinct(False),
            c.first << m.amount > 10,
            c.second << m.amount > c.first.amount * 2,
            c.third << m.amount > (c.first.amount + c.second.amount) / 2,
        )
        def detected(c):
            print("indistinct -> {0}".format(c.first.amount))
            print("           -> {0}".format(c.second.amount))
            print("           -> {0}".format(c.third.amount))

    post("indistinct", {"amount": 50})
    post("indistinct", {"amount": 200})
    post("indistinct", {"amount": 251})


def test_ruleset_distinct():

    with ruleset("distinct"):

        @when_all(
            c.first << m.amount > 10,
            c.second << m.amount > c.first.amount * 2,
            c.third << m.amount > (c.first.amount + c.second.amount) / 2,
        )
        def detected(c):
            print("distinct -> {0}".format(c.first.amount))
            print("         -> {0}".format(c.second.amount))
            print("         -> {0}".format(c.third.amount))

    post("distinct", {"amount": 50})
    post("distinct", {"amount": 200})
    post("distinct", {"amount": 251})
