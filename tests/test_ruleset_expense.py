from durable.lang import (
    ruleset,
    c,
    m,
    when_all,
    post,
    when_any,
    cap,
    count,
    post_batch,
    assert_fact,
    statechart,
    state,
    to,
    flowchart,
    stage,
    run,
    item,
)


def test_ruleset_expense0():

    with ruleset("expense0"):

        @when_all((m.subject == "approve") | (m.subject == "ok"))
        def approved(c):
            print("expense0-> Approved subject: {0}".format(c.m.subject))

    post("expense0", {"subject": "approve"})


def test_ruleset_expense1():

    with ruleset("expense1"):

        @when_any(
            all(c.first << m.subject == "approve", c.second << m.amount == 1000),  # type: ignore
            all(c.third << m.subject == "jumbo", c.fourth << m.amount == 10000),  # type: ignore
        )
        def action(c):
            if c.first:
                print(
                    "expense1-> Approved {0} {1}".format(
                        c.first.subject, c.second.amount
                    )
                )
            else:
                print(
                    "expense1-> Approved {0} {1}".format(
                        c.third.subject, c.fourth.amount
                    )
                )

    post("expense1", {"subject": "approve"})
    post("expense1", {"amount": 1000})
    post("expense1", {"subject": "jumbo"})
    post("expense1", {"amount": 10000})


def test_ruleset_expense2():

    with ruleset("expense2"):

        # this rule will trigger as soon as three events match the condition
        @when_all(count(3), m.amount < 100)
        def approve(c):
            print("expense2-> approved {0}".format(c.m))

        # this rule will be triggered when 'expense' is asserted batching at most two results
        @when_all(cap(2), c.expense << m.amount >= 100, c.approval << m.review == True)
        def reject(c):
            print("expense2-> rejected {0}".format(c.m))

    post_batch(
        "expense2",
        [
            {"amount": 10},
            {"amount": 20},
            {"amount": 100},
            {"amount": 30},
            {"amount": 200},
            {"amount": 400},
        ],
    )
    assert_fact("expense2", {"review": True})


def test_ruleset_expense3():

    with statechart("expense3"):

        # initial state 'input' with two triggers
        with state("input"):
            # trigger to move to 'denied' given a condition
            @to("denied")
            @when_all((m.subject == "approve") & (m.amount > 1000))
            # action executed before state change
            def denied(c):
                c.s.status = "expense3-> denied amount {0}".format(c.m.amount)

            @to("pending")
            @when_all((m.subject == "approve") & (m.amount <= 1000))
            def request(c):
                c.s.status = "expense3-> requesting approve amount {0}".format(
                    c.m.amount
                )

        # intermediate state 'pending' with two triggers
        with state("pending"):

            @to("approved")
            @when_all(m.subject == "approved")
            def approved(c):
                c.s.status = "expense3-> expense approved"

            @to("denied")
            @when_all(m.subject == "denied")
            def denied(c):
                c.s.status = "expense3-> expense denied"

        # 'denied' and 'approved' are final states
        state("denied")
        state("approved")

    # events directed to default statechart instance
    print(post("expense3", {"subject": "approve", "amount": 100})["status"])
    print(post("expense3", {"subject": "approved"})["status"])

    # events directed to statechart instance with id '1'
    print(post("expense3", {"sid": 1, "subject": "approve", "amount": 100})["status"])
    print(post("expense3", {"sid": 1, "subject": "denied"})["status"])

    # events directed to statechart instance with id '2'
    print(post("expense3", {"sid": 2, "subject": "approve", "amount": 10000})["status"])


def test_ruleset_expense4():

    with flowchart("expense4"):
        # initial stage 'input' has two conditions
        with stage("input"):
            to("request").when_all((m.subject == "approve") & (m.amount <= 1000))
            to("deny").when_all((m.subject == "approve") & (m.amount > 1000))

        # intermediate stage 'request' has an action and three conditions
        with stage("request"):

            @run
            def request(c):
                print("expense4-> requesting approve")

            to("approve").when_all(m.subject == "approved")
            to("deny").when_all(m.subject == "denied")
            # reflexive condition: if met, returns to the same stage
            to("request").when_all(m.subject == "retry")

        with stage("approve"):

            @run
            def approved(c):
                print("expense4-> expense approved")

        with stage("deny"):

            @run
            def denied(c):
                print("expense4-> expense denied")

    # events for the default flowchart instance, approved after retry
    post("expense4", {"subject": "approve", "amount": 100})
    post("expense4", {"subject": "retry"})
    post("expense4", {"subject": "approved"})

    # events for the flowchart instance '1', denied after first try
    post("expense4", {"sid": 1, "subject": "approve", "amount": 100})
    post("expense4", {"sid": 1, "subject": "denied"})

    # event for the flowchart instance '2' immediately denied
    post("expense4", {"sid": 2, "subject": "approve", "amount": 10000})

    with ruleset("expense5"):

        @when_all(
            c.bill << (m.t == "bill") & (m.invoice.amount > 50),
            c.account << (m.t == "account")
            & (m.payment.invoice.amount == c.bill.invoice.amount),
        )
        def approved(c):
            print("expense5-> bill amount: {0}".format(c.bill.invoice.amount))
            print(
                "expense5-> account payment amount: {0}".format(
                    c.account.payment.invoice.amount
                )
            )

    post("expense5", {"t": "bill", "invoice": {"amount": 100}})
    post("expense5", {"t": "account", "payment": {"invoice": {"amount": 100}}})


def test_ruleset_expense6():

    with ruleset("expense6"):

        @when_any(
            (m.subject == "approve") & (m.amount == 1000),
            (m.subject == "jumbo") & (m.amount == 10000),
        )
        def action(c):
            print("expense6 matched")

    post("expense6", {"subject": "approve", "amount": 1000})
    post("expense6", {"subject": "jumbo", "amount": 10000})


def test_ruleset_expense7():

    with ruleset("expense7"):

        @when_any(
            m.observations.anyItem(item.matches("a"))
            & m.observations.anyItem(item.matches("b")),
            m.observations.anyItem(item.matches("c"))
            & m.observations.anyItem(item.matches("d")),
        )
        def match(c):
            print("expense7 matched")

    post("expense7", {"observations": ["a", "b"]})
    post("expense7", {"observations": ["c", "d"]})
